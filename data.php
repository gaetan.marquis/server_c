<?php include "menu.php" ?>

<div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main" >
                <div class="col-sm-12 col-md-12 well" id="content">
                    <h1>Data :</h1>
                </div>
            </div>

            <?php include "database.php" ?>

            <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">ID</th>
                        <th class="th-sm">Date</th>
                        <th class="th-sm">Joystick_X</th>
                        <th class="th-sm">Joystick_Y</th>
                        <th class="th-sm">Light</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while($row = mysqli_fetch_array($req)){ ?>
                    <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['date_time']; ?></td>
                        <td><?php echo $row['joystickX']; ?></td>
                        <td><?php echo $row['joystickY']; ?></td>
                        <td><?php echo $row['light']; ?></td>
                    </tr>
                <?php }   
                mysqli_close($connexion);  
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div><!-- /#wrapper -->

<script type="text/javascript">
$(document).ready(function () {
  $('#dtBasicExample').DataTable({
    "pagingType": "full_numbers"
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>

</body>
</html>