<?php include "menu.php" ?>

<div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main" >
                <div class="col-sm-12 col-md-12 well" id="content">
                    <h1>Informations :</h1>
                </div>
            </div>
           <p> Ce projet de Programmation en C met en oeuvre une RaspberryPi collectant et transmettant des données vers une BDD, 
           pour finalement les afficher dans un tableau sur un site web.</p>
           <p>La récolte et la transmission de données ont été réalisées avec le langage C. L'affichage sur le site web est réalisé en HTML, PHP & JavaScript.</p>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div><!-- /#wrapper -->
</body>
</html>