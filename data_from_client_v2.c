#if defined (WIN32)
    #include <winsock2.h>
    typedef int socklen_t;
#elif defined (linux)
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define closesocket(s) close(s)
    typedef int SOCKET;
    typedef struct sockaddr_in SOCKADDR_IN;
    typedef struct sockaddr SOCKADDR;
#endif
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#define BUF_SIZE 128
#define PORT 23

//Connexion à la BDD localhost
//gcc -o output $(mysql_config --cflags) data.c $(mysql_config --libs)
const char *host = "localhost";
const char *user = "_MYSQL_USERNAME_";
const char *pass = "_MYSQL_PASSWORD_";
const char *dbname = "database";
unsigned int port = 3306;
const char *unix_socket = NULL;
unsigned int flags = 0;

int main(void)
{
    #if defined (WIN32)
        WSADATA WSAData;
        int erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
    #else
        int erreur = 0;
    #endif
 
    SOCKET sock;
    SOCKADDR_IN sin;
    SOCKET csock;
    SOCKADDR_IN csin;
    socklen_t recsize = sizeof(csin);  // sizeof(sin) directly add in function
    int sock_err;

    unsigned char strRecvData[BUF_SIZE];
    char strSendOk[2];
    char delim[] = "/";

    strcpy(strSendOk, "S");

    /* Si les sockets Windows fonctionnent */
    if(!erreur)
    {
        sock = socket(AF_INET, SOCK_STREAM, 0);
 
        /* Si la socket est valide */
        if(sock != INVALID_SOCKET)
        {
            printf("Socket %d is opened in TCP/IP\n", sock);
 
            /* Configuration */
            sin.sin_addr.s_addr = htonl(INADDR_ANY);  /* Adresse IP automatique */
            sin.sin_family = AF_INET;  /* Protocole familial (IP) */
            sin.sin_port = htons(PORT);  /* Listage du port */
            sock_err = bind(sock, (SOCKADDR*)&sin, sizeof(sin));
 
            /* Si la socket fonctionne */
            if(sock_err != SOCKET_ERROR)
            {
			/* Démarrage du listage (mode server) */
                sock_err = listen(sock, 5);
                printf("Port listing %d...\n", PORT);
 
                /* Si la socket fonctionne */
                if(sock_err != SOCKET_ERROR)
                {
                    /* Attente pendant laquelle le client se connecte */
                    printf("Wait for client connection on port %d...\n", PORT);        
 
                    csock = accept(sock, (SOCKADDR*)&csin, &recsize);
                    printf("A client is connected with socket %d of %s:%d\n", csock, inet_ntoa(csin.sin_addr), htons(csin.sin_port));
                    MYSQL *connexion;
				    connexion = mysql_init(NULL);

				    if(!(mysql_real_connect(connexion, host, user, pass, dbname, port, unix_socket, flags))){
				        fprintf(stderr, "\nError: %s [%d]\n", mysql_error(connexion), mysql_errno(connexion));
				        exit(1);
				    }
				    printf("Connexion DATABASE réussie !\n");
 
				    while(1)
				    {
		                if(recv(csock, &strRecvData, sizeof(strRecvData), 0) != SOCKET_ERROR)
				    	{
							printf("Data received : %s\n", (char *)strRecvData);

							char *splitedData = strtok(strRecvData, delim);
							int i = 0;
			    			int tab[3];

							while(splitedData != NULL)
							{
								printf("%i\n", atoi(splitedData)); //conversion en int
								tab[i] = atoi(splitedData);
								splitedData = strtok(NULL, delim);
								i++;
							}

							char query[255];
						    sprintf(query, "INSERT INTO `data_table` (`joystickX`,`joystickY`,`light`) VALUES (%i, %i, %i)", tab[0], tab[1], tab[2]);
						    printf(query);    
						    mysql_query(connexion, query);
						   
						    //MYSQL_RES *res;
						    //MYSQL_ROW row;
						    //res = mysql_store_result(connexion);
						    
						    //while(row = mysql_fetch_row(res))
						 	//printf("%s\t%s\t%s\t%s\t%s\n", row[0], row[1], row[2], row[3], row[4]);
						    
						    //mysql_free_result(res);
						}
						else
						{
							printf("Receiving data error\n");
						}

						sock_err = send(csock, &strSendOk, sizeof(strSendOk), 0);

						if(sock_err != SOCKET_ERROR)
						{
							printf("Ready for reception\n");
						}
						else
						{
							printf("Sending data error");
						}
				 	}
				 	mysql_close(connexion);
		    	}

		    /* Il ne faut pas oublier de fermer la connexion (fermée dans les deux sens) */
                    shutdown(csock, 2);  // NEW
            }
        }
 
        /* Fermeture de la socket */
        printf("Fermeture de la socket...\n");
        closesocket(sock);
        printf("Fermeture du serveur terminee\n");
    }

    #if defined (WIN32)
        WSACleanup();
    #endif

return EXIT_SUCCESS;
} 


