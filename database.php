<?php  
/* Connect to a MySQL database using driver invocation */
$database = 'database';
$user = 'diego';
$password = '**********';
$connexion = mysqli_connect("localhost", $user, $password, $database);
if (!$connexion) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
echo "Success: A proper connection to MySQL was made! The database is great." . PHP_EOL;
echo "Host information: " . mysqli_get_host_info($connexion) . PHP_EOL;
$query = 'SELECT * FROM data_table';   
$req = mysqli_query($connexion, $query);
?>
