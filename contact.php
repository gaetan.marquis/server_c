<?php include "menu.php" ?>

<div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main" >
                <div class="col-sm-12 col-md-12 well" id="content">
                    <h1>Contacts :</h1>
                </div>
            </div>
            <p>Ce projet a été réalisé par :</p>
            <p>Gaëtan MARQUIS : gaetan.marquis@edu.ece.fr</p>
            <p>Diégo BOUHANICHE: diego.bouhaniche@edu.ece.fr</p>
            <p>----------------------------------------------------------------------------------------------</p>
            <p>Professeur référent :</p>
            <p>Mr.TOUATI : touati@ai.univ-paris8.fr</p>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div><!-- /#wrapper -->
</body>
</html>