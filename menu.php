<?php include "header.php" ?>

<body>
<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <a href="index.php"> <img  src="logo.png" height="50" width="200" alt="LOGO">
            </a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
               <a href="https://www.ece.fr/"><i class="fas fa-archway"></i> École d'ingénieur ECE</a>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="navbar-collapse navbar-ex1">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="data.php"><i class="fa fa-fw fa-search"></i> Data</a>
                </li>
                <li>
                    <a href="contact.php"><i class="fa fa-fw fa-paper-plane-o"></i> Contact</a>
                </li>
                <li>
                    <a href="infos.php"><i class="fa fa-fw fa fa-question-circle"></i> Infos</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>